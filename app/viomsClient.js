import Settings from './settings'

export const viomsHttpRequest = ({ url, token, method = 'GET', body = undefined }) => {
  const headers = { 'Content-type': 'application/json' }
  if (token)
    headers['Authorization'] = `Bearer ${token}`
  return fetch(`${Settings.backendUrl}${url}`, { method, headers, body })
}
