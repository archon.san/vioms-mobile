import React from 'react'
import { ActivityIndicator } from 'react-native'

const StyledActivityIndicator = () => (
  <ActivityIndicator size="large" color="gray" />
)

export default StyledActivityIndicator
