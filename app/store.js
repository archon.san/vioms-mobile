import { configureStore } from '@reduxjs/toolkit'

import authenticationReducer from '../features/authentication/authenticationSlice'
import emailListsReducer from '../features/emailLists/emailListsSlice'
import mailingsReducer from '../features/mailings/mailingsSlice'
import mailingReducer from '../features/mailing/mailingSlice'

export default configureStore({
  reducer: {
    authentication: authenticationReducer,
    emailLists: emailListsReducer,
    mailings: mailingsReducer,
    mailing: mailingReducer,
  },
})
