import React from 'react'
import { useSelector } from 'react-redux'
import { createStackNavigator } from '@react-navigation/stack'

import SignInScreen from '../../features/authentication/signInScreen'
import ProfileScreen from '../../features/authentication/profileScreen'

const Stack = createStackNavigator()

const AuthenticationStack = () => {
  const signedIn = useSelector(state => !!state.authentication.token)
  return (
    <Stack.Navigator>
      {signedIn ? (
        <Stack.Screen name='Profile' component={ProfileScreen} options={{ title: 'Профиль' }}/>
      ) : (
        <Stack.Screen name='SignIn' component={SignInScreen} options={{ title: 'Войти' }}/>
      )}
    </Stack.Navigator>
  )
}

export default AuthenticationStack
