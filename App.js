import React from 'react'
import { SafeAreaProvider } from 'react-native-safe-area-context'
import { Provider } from 'react-redux'
import AppLoading from 'expo-app-loading'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { LogBox } from 'react-native'
import { StatusBar } from 'expo-status-bar'

import store from './app/store'
import Navigator from './app/navigation/Navigator'
import { restoreToken } from './features/authentication/authenticationSlice'

LogBox.ignoreLogs([
  'Native splash screen is already hidden',
])

export default function App() {
  const [isLoading, setIsLoading] = React.useState(true)

  const loadToken = () => {
    AsyncStorage.getItem('token')
      .then(data => store.dispatch(restoreToken({ token: data })))
  }

  if (isLoading) {
    return <AppLoading startAsync={loadToken} onFinish={() => setIsLoading(false)} onError={console.warn} />
  }

  return (
    <Provider store={store}>
      <SafeAreaProvider>
        <StatusBar />
        <Navigator />
      </SafeAreaProvider>
    </Provider>
  )
}
