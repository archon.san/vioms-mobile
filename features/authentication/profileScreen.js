import React from 'react'
import { Alert, Button, ScrollView } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { useDispatch, useSelector } from 'react-redux'
import { signOut } from './authenticationSlice'

import StyledActivityIndicator from '../../app/components/StyledActivityIndicator'

const ProfileScreen = () => {
  const authenticationStatus = useSelector(state => state.authentication.status)
  const error = useSelector(state => state.authentication.error)
  const dispatch = useDispatch()

  React.useEffect(() => {
    if (authenticationStatus === 'failed')
      Alert.alert('Ошибка', error, [{ text: 'OK' }])
  }, [authenticationStatus, dispatch])


  let content
  if (authenticationStatus === 'loading')
    content = <StyledActivityIndicator />
  else
    content = <Button onPress={() => dispatch(signOut())} title='Выйти' />

  return (
    <SafeAreaView>
      <ScrollView>
        { content }
      </ScrollView>
    </SafeAreaView>
  )
}

export default ProfileScreen
